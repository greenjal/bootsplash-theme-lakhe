# bootsplash-theme-lakhe

Kernel Bootsplash theme for Manjaro Linux using Lakhe logo

# Installation:

~~The package is in [AUR](https://aur.archlinux.org/packages/bootsplash-theme-lakhe/), so just run
`yaourt -S bootsplash-theme-lakhe` or~~ Not Yet

Clone this repo, build the package with `makepkg` and then install it

# After installation:

- append `bootsplash-lakhe` hook in the end of HOOKS string of `/etc/mkinitcpio.conf`
- add `bootsplash.bootfile=bootsplash-themes/lakhe/bootsplash` in kernel boot arguments
- run `sudo mkinitcpio -P`
